from django.test import TestCase, Client
from authentication.models import SLA, SLADatos
from authentication.models import Tagente, Tgrupo, UserNoname
from authentication.models import TagenteDatos, TData, TagenteModulo
from django.http import HttpResponse, HttpResponseServerError, JsonResponse
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'noname.settings'

# Create your tests here.


class TestsNode(TestCase):
    def test_nodeBash(self):

        c = Client()
        response = c.post('/node/node-bash/', {'command': 'ls'})
        self.assertEqual(response.status_code, 200)

    def test_nodePerl(self):
        c = Client()
        response = c.post(
            '/node/node-perl/', {'command': "print 'Estamos en Perl'"})
        self.assertEqual(response.status_code, 200)

    def test_shellDjango(self):
        c = Client()
        response = c.post(
            '/node/shell-django/',  {'command': "print 'Estamos en Django'"})
        self.assertEqual(response.status_code, 200)
