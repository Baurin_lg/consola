from django.shortcuts import render
import json
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseServerError
from django.http import JsonResponse


from django.views.decorators.csrf import csrf_exempt
import os

# Create your views here.



@csrf_exempt
def shellDjango(request):

    print 'Django execute Django Shell command:\n'
    print '''   -->$python -c "import django;%s"''' % request.POST['command']
    cmd = '''python -c "import django;%s"''' % request.POST['command']
    responseCommand = os.popen(cmd)
    responseRead = responseCommand.read()
    print '\n', responseRead
    response = JsonResponse({
        'command': request.POST['command'],
        'responseCommand': responseRead,
        'currentPath':os.getcwd(),

        })
    return HttpResponse(response.content)



@csrf_exempt
def nodeBash(request):

    print 'Django execute Bash command:\n'
    print '   -->$', request.POST['command']
    command = request.POST['command']
    ret = commandManager(command);
    return HttpResponse(ret.content)


def commandManager(cmd):

    if (cmd.find('cd ') == 0):
        path = cmd.replace('cd ','')
        directoryManager(path)
        responseCommand = os.popen('ls')
    else:
        responseCommand = os.popen(cmd)
    
    responseRead = responseCommand.read()
    # print '\n', responseRead
    # print str(os.getcwd())
    response = JsonResponse({
        'command': cmd,
        'responseCommand': responseRead,
        'currentPath':str(os.getcwd()),
        'arrayResponse' : responseRead.split('\n')
        })
    return response
       
    
def directoryManager(path):
    os.chdir(path)