from django.conf.urls import patterns, include, url
from django.contrib import admin
# from slas.views import NodeConection

urlpatterns = patterns('',

                       # Examples:
                       # url(r'^$', 'noname.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),


                       url(r'^async-slas/$', 'fromNode.views.getRealTimeSLAS'),
                       url(r'^node-bash/$', 'fromNode.views.nodeBash'),
                       url(r'^node-perl/$', 'fromNode.views.nodePerl'),
                       url(r'^shell-django/$', 'fromNode.views.shellDjango'),
                       url(r'^validateKey/$', 'fromNode.views.validateFromNode'),

                       #url(r'^async_slas/$', NodeConection.getRealTimeSLAS()),


                       # url(r'^migrate', 'slas.views.migrate'),



                       )
