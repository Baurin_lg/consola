import os
from crontab import CronTab
import sys
sys.path.insert(0, '../../noname')
from noname.settings import SESSION_COOKIE_AGE

class changerKey(object):
    def __init__(self):
        self.keyfile = "./keyfiles/djangoJS.pass"

    def generateKey(self):
        return os.popen('openssl rand -base64 32').read()

    def ReadKeyFile(self):
        return open("./keyfiles/djangoJS.pass","r")

    def WriteKeyFile(self):
        return open("./keyfiles/djangoJS.pass","w")
    
    def closeKeyFile(self,file):
        file.close()

    def changeKeys(self):
        file = self.ReadKeyFile()
        lines = file.readlines()
        for i in range(0,len(lines)-1):
            if lines[i].find('publicKey') != -1 :
                lines[i] = lines[i].split(' ')[0]+" "+self.generateKey()
            if lines[i].find('controlString') != -1:
                lines[i] = lines[i].split(' ')[0]+" "+self.generateKey()
        self.closeKeyFile(file)
        file = self.WriteKeyFile()
        for line in lines:
            file.write(line)
        self.closeKeyFile(file)


if __name__ == '__main__':
    changer = changerKey()
    changer.changeKeys()
