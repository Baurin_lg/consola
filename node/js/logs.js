exports.print_cliente_node=function (){
      console.log('Node recibe peticion del cliente');
      console.log('      _____     ');
      console.log('      | D |     ');
      console.log('      -----     ');
      console.log(' _____      _____ ');
      console.log(' | C | ---> | Nj| ');
      console.log(' -----      ----- ')
}


exports.print_node_django= function(){
      console.log('Node envia peticion a Django');
      console.log('      _____       ');
      console.log('      | D |<---|  ');
      console.log('      -----    |  ');
      console.log(' _____      ___|_ ');
      console.log(' | C |      | Nj| ');
      console.log(' -----      ----- ')
}
exports.print_stopPolling = function(chart_id){
     console.log('Parando Polling con socket_id = '+chart_id);
      console.log('      ______________       ');
      console.log('      | STOPING ̣̣̣ |      ');
      console.log('      --------------       ');
}

exports.shellBashtoDjango = function(command){
      console.log('Django ejecuta:');
      console.log(' -->(Bash)$'+command);
}
exports.shellPerltoDjango = function(command){
      console.log('Django ejecuta:');
      console.log(' -->(Bash)$'+command);
}
exports.shellDjango = function(command){
      console.log('Django ejecuta:');
      console.log(' -->(python) $'+command);
}
exports.print_django_node =function(){
      console.log('Node recibe peticion desde Django');
      console.log('      _____       ');
      console.log('      | D |       ');
      console.log('      -----       ');
      console.log(' _____   |  _____ ');
      console.log(' | C |   |->| Nj| ');
      console.log(' -----      ----- ')
}


exports.print_node_cliente=function(){
      console.log('      _____       ');
      console.log('      | D |       ');
      console.log('      -----       ');
      console.log(' _____      _____ ');
      console.log(' | C | <----| Nj| ');
      console.log(' -----      ----- ')
}

exports.changedPoll=function(newData){
      console.log('NEW DATA:');
      console.log(newData);
}

exports.print_polling=function(table_name){
      console.log('  --> Polling table '+table_name+'...');
}

exports.print_inserting=function(table_name){
      console.log('  --> Data inserting in '+table_name+'...');
}

exports.print_updating =function(table_name){
      console.log('  --> Updating '+table_name+'...');
}

exports.print_custom=function(name_method,destiny_app,url){
      console.log('  --> Creating method '+ name_method +' in '+destiny_app+' ...');
}

exports.print_request_to_django=function(url){
      console.log('  --> Requesting to Django url '+url+'...');
}