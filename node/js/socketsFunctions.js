///////////////////////////////////////
////////////// DjangoJS ///////////////
///////////////////////////////////////

// Intercomunication tool for connect 
// Node and Django. 


var cluster     =     require("cluster");
var events      =     require('events'); 
var EmitEvents  =     events.EventEmitter;

var eventHandler_=     new EmitEvents();
var events      =     require('events'); 
var EmitEvents  =     events.EventEmitter;

var numCPUs = require('os').cpus().length;
var logs        =     require('./logs.js');
var querystring =     require('querystring');
var chalk = require('chalk');
var n_connected = 0;
var ip,
    port_node,
    port_django;



exports.createConection=function(http,arg){
    // console.log(arg);
    port_django = arg.port_django || 8000;
    port_node = arg.port_node || 3000;
    ip = arg.ip || 'localhost';
    
    var server = http.createServer().listen(port_node);
    var io = require('socket.io').listen(server);
    io.on('connection',function(socket){
      n_connected += 1;
      if (n_connected == 1) console.log(chalk.white('Conexion [OK]'));
      socket.on("error",function(err){console.log(err)})
      socket.emit("reconect");
      socket.on("console",function(data){
      })

      socket.on('listen',function(data){
      });

      socket.on('shell django',function(data){
        shellDjango(io,data,http,server,ip,port_django);
      });

      socket.on('shell bash',function(data){
        shellBash(io,data,http,server,ip,port_django);
      });
    })
    io.on('disconnet',function(){
      if (n_connected == 1) console.log(chalk.red('Nadie Conectado.'));
      n_connected -= 1;
    })
    server.on('error', function (er) {
      console.log("error server node"+er)
    })
}






var shellDjango=function(io,argv,http,server){
      
      var values=querystring.stringify(argv);
      var options={
        hostname: ip ,
        port: port_django ,
        path:'/node/shell-django/',
        method:'POST',
        headers:{
          'Content-Type':'application/x-www-form-urlencoded',
          'Content-Length':values.length
        }
      }
      var request=http.request(options, function(response){
        response.setEncoding('utf8');
        response.on('data',function(data){
          //Aqui vienen los datos de django
          var json=JSON.parse(data);
          io.emit('return shelldjango', json);
        });
      });
      request.write(values);
      request.end();
 };


 var shellBash=function(io,argv,http,server,arg){
    
      var values=querystring.stringify(argv);
      var options={
        hostname: ip,
        port: port_django,
        path:'/node/node-bash/',
        method:'POST',
        headers:{
          'Content-Type':'application/x-www-form-urlencoded',
          'Content-Length':values.length
        }
      }

      var request=http.request(options, function(response){
        response.setEncoding('utf8');
        response.on('data',function(data){
          //Aqui vienen los datos de django
          var json=JSON.parse(data);
          io.emit('return bash', json);
        });
      });
      request.write(values);
      request.end();
 };
